using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ChatCore;

public class Client : MonoBehaviour
{
    public InputField IPInput = null;
    public InputField NameInput = null;
    public InputField MessageInput = null;
    private ChatClient m_client = null;
    private bool isConnect = false;
    // Start is called before the first frame update
    void Start()
    {

        m_client = new ChatClient();
        isConnect = m_client.Connect("127.0.0.1", 4099);
    }

    // Update is called once per frame
    void Update()
    {
        
             m_client.Refresh();
             var rt = m_client.GetMessages();
             foreach (var message in rt)
             {
                 Debug.Log($"{message.Key}: { message.Value}");
             }
       
       
    }

    public void OnConnectClick()
    {
        Debug.Log("IP: " + IPInput.text);
        Debug.Log("Name: " + NameInput.text);
        m_client.Connect(IPInput.text,4099);
        m_client.SetName(NameInput.text);
    }

    public void OnSendClick()
    {
        Debug.Log("Message: " + MessageInput.text);
        m_client.SendMessage(MessageInput.text);
        MessageInput.text = "";
        
    }

}
