﻿using System;
using System.Text;
using System.Collections.Generic;
namespace ChatCoreTest
{
  internal class Program
  {
    private static byte[] m_PacketData;
    private static uint m_Pos;
    //public static List<Tuple<bool, int, string>> wrightIn;
    public static void Main(string[] args)
    {
      //wrightIn = new List<Tuple<bool, int, string>>();
      m_PacketData = new byte[1024];
      m_Pos = 0;

            Write(109);
            Write(109.99f);
            Write("Hello!");
            
            Console.Write($"Output Byte array(length:{m_Pos}): ");
            //for (var i = 0; i < m_Pos; i++)
            //{
            //    Console.Write(m_PacketData[i] + ",");
            //}

            var deW = deWrite(m_PacketData);

            Console.WriteLine($"長度：{m_Pos} ：" +
                $"{BitConverter.ToInt32(deW, 0)}," +
                $"{BitConverter.ToSingle(deW, 4)}," +

                $"{Encoding.Unicode.GetString(deW, 12, 12)}");
        }

    // write an integer into a byte array
    private static bool Write(int i)
    {
            // convert int to byte array
            //var dataType = new Tuple<bool, int,string>(true, 4,"int");
            //wrightIn.Add(dataType);
            var bytes = BitConverter.GetBytes(i);
            _Write(bytes);
      return true;
    }

    // write a float into a byte array
    private static bool Write(float f)
    {
            // convert int to byte array
            //var dataType = new Tuple<bool, int,string>(true, 4,"float");
            //wrightIn.Add(dataType);
            var bytes = BitConverter.GetBytes(f);
      _Write(bytes);
      return true;
    }

    // write a string into a byte array
    private static bool Write(string s)
    {
      // convert string to byte array
      var bytes = Encoding.Unicode.GetBytes(s);

      // write byte array length to packet's byte array
      if (Write(bytes.Length) == false)
      {
                //var fdataType = new Tuple<bool, int,string>(false, 0,"none");
                //wrightIn.Add(fdataType);
                return false;
            }
            //var dataType = new Tuple<bool, int,string>(true, bytes.Length,"string");
            //wrightIn.Add(dataType);
            _Write(bytes);
      return true;
    }

    // write a byte array into packet's byte array
    private static void _Write(byte[] byteData)
    {
            // converter little-endian to network's big-endian
      
      if (BitConverter.IsLittleEndian)
      {
        Array.Reverse(byteData);
      }
      byteData.CopyTo(m_PacketData, m_Pos);
      m_Pos += (uint)byteData.Length;
     
    }
        static private byte[] deWrite(byte[] packet)
        {
            Array.Reverse(packet, 0, 4);
            Array.Reverse(packet, 4, 4);
            Array.Reverse(packet, 8, 4);
            Array.Reverse(packet, 12, 12);
            Console.WriteLine("\n");

            //var i = 0;
            //foreach (var item in wrightIn)
            //{
            //    Array.Reverse(packet, i, item.Item2);                     
            //    switch (item.Item3)
            //    {
            //        case "int":
            //            Console.WriteLine($"{BitConverter.ToInt32(packet, i)}");
            //            break;
            //        case "float":
            //            Console.WriteLine($"{BitConverter.ToSingle(packet, i)}");
            //            break;
            //        case "string":
            //            Console.WriteLine($"{Encoding.Unicode.GetString(packet,i,item.Item2)}");
            //            break;
            //        default:
            //            break;
            //    }
            //    i += item.Item2;
            //}
            return packet;

        }

       
     
  }
}
